<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('users')->insert([
          	'role_id'=>'1',
            'name' => 'Mr. Admin',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);

          DB::table('users')->insert([
          	'role_id'=>'2',
            'name' => 'Mr. Author',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
        ]);
    }
}
